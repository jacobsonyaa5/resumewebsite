'use strict';

/**
 * @ngdoc function
 * @name yakoApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the yakoApp
 */
angular.module('yakoApp')
  .controller('ContactCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
