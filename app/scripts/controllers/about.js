'use strict';

/**
 * @ngdoc function
 * @name yakoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the yakoApp
 */
angular.module('yakoApp')
    .controller('AboutCtrl', function($scope, $location, $anchorScroll) {
        $scope.jumpToLocation = function(key) {
            $location.hash(key);
            $anchorScroll();
        };
    });
