'use strict';

/**
 * @ngdoc overview
 * @name yakoApp
 * @description
 * # yakoApp
 *
 * Main module of the application.
 */
angular
    .module('yakoApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap'
    ])
    .config(function($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl'
            })
            .when('/projects', {
                templateUrl: 'views/projects.html',
            })
            .when('/techfetch', {
                templateUrl: 'views/techfetch.html',
            })
            .when('/contact', {
                templateUrl: 'views/contact.html',
                 controller: 'ContactCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });

        // use the HTML5 History API
        $locationProvider.html5Mode(true);
    })
    .controller('HeaderCtrl', function($scope, $location) {
        $scope.isActive = function(viewLocation) {
            return viewLocation === $location.path();
        };
    });
